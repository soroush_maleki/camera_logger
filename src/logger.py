#!/usr/bin/env python

import rospy
import os
import cv2

import message_filters
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image


class CameraLogger:
    def __init__(self, left_image_topic_name, right_image_topic_name, list_file_name, save_path, reduction_factor, first_picture_start):
        self.left_image_topic_name = left_image_topic_name
        self.right_image_topic_name = right_image_topic_name
        self.list_file_name = list_file_name

        self.left_save_path = os.path.join(save_path, "left")
        self.right_save_path = os.path.join(save_path, "right")

        self.img_l_sub = message_filters.Subscriber(self.left_image_topic_name, Image, queue_size=1000)
        self.img_r_sub = message_filters.Subscriber(self.right_image_topic_name, Image, queue_size=1000)

        ts = message_filters.TimeSynchronizer([self.img_l_sub, self.img_r_sub], queue_size=1000)
        ts.registerCallback(self.image_callback)

        self.bridge = CvBridge()
        self.red_factor = reduction_factor
        self.count = first_picture_start * reduction_factor

    def image_callback(self, img_left, img_right):
        print("Image received.")
        try:
            left_cv2_img = self.bridge.imgmsg_to_cv2(img_left, "bgr8")
            right_cv2_img = self.bridge.imgmsg_to_cv2(img_right, "bgr8")

        except CvBridgeError as e:
            print(e)
        else:
            self.count += 1
            if self.count % self.red_factor == 0:
                image_number = int(self.count / self.red_factor)
                print("writing")
                image_name = "{:04}".format(image_number) + '.jpg'
                left_image_path = os.path.join(self.left_save_path, image_name)
                right_image_path = os.path.join(self.right_save_path, image_name)
                cv2.imwrite(left_image_path, left_cv2_img)
                cv2.imwrite(right_image_path, right_cv2_img)
                with open(self.list_file_name, 'a') as f:
                    f.write("left/" + image_name + " " + "right/" + image_name + "\n")

            else:
                pass


if __name__ == "__main__":
    left_image_topic_name = "/camera/infra1/image_rect_raw"
    right_image_topic_name = "/camera/infra2/image_rect_raw"
    list_file_name = "/home/soroush/catkin_ws/src/camera_sim_logger/src/logs/list/camera_list.txt"
    save_path = "/home/soroush/catkin_ws/src/camera_sim_logger/src/logs"
    reduction_factor = 10
    first_picture_start = 0
    rospy.init_node("image_logger")
    logger = CameraLogger(left_image_topic_name, right_image_topic_name, list_file_name, save_path, reduction_factor,
                          first_picture_start)
    rospy.spin()
